import java.util.Scanner;

public class OXGame {

	public static char table[][] = { { '-', '-', '-' }, { '-', '-', '-' }, { '-', '-', '-' }

	};
	public static char pattern[] = { 'x' };
	public static boolean win = false;
	public static boolean fair = false;

	public static void main(String[] args) {

		showStart();
		int count = 0;
		while (true) {

			if (count % 2 == 0)
				pattern[0] = 'x';
			else
				pattern[0] = 'o';

			showBord();
			showTurn();
			input();
			checkWin();

			if (win == true) {

				showBord();
				showWin();
				showEnd();
				break;
			}

			if (fair == true) {
				showBord();
				showFair();
				showEnd();
				break;
			}

			count++;
		}

	}

	public static void input() {
		Scanner kb = new Scanner(System.in);
		System.out.print("Plaease input Row Col: ");
		int row = kb.nextInt();
		int col = kb.nextInt();
		checkInput(row, col);

	}

	public static void checkInput(int r, int c) {
		if (r <= 3 && r > 0) {
			if (c <= 3 && c > 0) {
				table[r - 1][c - 1] = pattern[0];
			}
		} else {
			System.out.println("Please Enter numbers 1-3");
			input();
		}
	}

	public static void checkFair() {
		int count = 0;
		for (int row = 0; row < table.length; row++) {
			for (int col = 0; col < table.length; col++) {
				if (table[row][col] == '-') {
					count++;
					
				}

			}

		}

		if (count == 0) {
			fair = true;
		}

	}

	public static void checkWin() {

		checkWinRow();
		checkWinCol();
		checkWinDiagonal();
		checkFair();

	}

	public static void checkWinRow() {
		int count = 0;

		for (int row = 0; row < table.length; row++) {
			if (table[row][0] == pattern[0] && table[row][1] == pattern[0] && table[row][2] == pattern[0]) {
				count++;

			}

		}

		if (count == 1) {
			win = true;

		}

	}

	public static void checkWinCol() {
		int count = 0;

		for (int col = 0; col < table.length; col++) {
			if (table[0][col] == pattern[0] && table[1][col] == pattern[0] && table[2][col] == pattern[0]) {
				count++;

			}

		}

		if (count == 1) {
			win = true;
		}

	}

	public static void checkWinDiagonal() {

		int count = 0;
		int row = 0;

		for (row = 0; row > table.length; row++)

			if (table[0][0] == pattern[0] && table[1][1] == pattern[0] && table[2][2] == pattern[0])
				count++;

		if (table[0][2] == pattern[0] && table[1][1] == pattern[0] && table[2][0] == pattern[0])
			count++;

		if (count == 1) {
			win = true;

		}

	}

	public static char showBord() {

		System.out.println("-----------------------------");
		System.out.println("  1 2 3");

		for (int row = 0; row < table.length; row++) {
			System.out.print((row + 1) + " ");
			for (int col = 0; col < table.length; col++) {
				System.out.print(table[row][col] + " ");
			}
			System.out.println();
		}
		System.out.println("-----------------------------");

		return 0;

	}

	public static void showEnd() {

		System.out.println("End Game");
	}

	public static void showTurn() {

		System.out.println("Turn " + pattern[0]);

	}

	public static void showStart() {

		System.out.println("Welcome to OX Game");

	}

	public static void showWin() {

		if (win = true) {
			System.out.println(pattern[0] + " Win");
		}

	}

	public static void showFair() {
		System.out.println("FAIR ");
	}

}
